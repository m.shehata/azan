require 'net/http'
require 'json'
require 'tod'

class AzanCoordinator
  attr_accessor :logger

  def initialize configs, logger
    @updated_at = nil
    @calendar = nil
    @configs = configs
    @logger = logger
  end

  def update_prayer_times
    uri = URI.parse("https://mawaqit.net/en/nasr-amsterdam")

    res = nil
    Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
      request = Net::HTTP::Get.new uri
      res = http.request request
    end

    data = res.body.lines.select{|l| l.include? 'confData'}.first[/{.*/].split(";").first
    @calendar = JSON.parse(data)["calendar"]
  end

  def prayer_times
    times = []
    @calendar[Time.now.month - 1][Time.now.day.to_s].each_with_index do |t, i|
      config = @configs[:salawat][i]
      time = Tod::TimeOfDay.parse(t)
      times[i] = {
        time: time,
        volume: config[:volume] ? config[:volume] : @configs[:default_volume],
        before_reminder_at: config[:before_reminder] ? (time - config[:before_reminder] * 60) : nil
      }
    end
    times
  end

  def sound_file_path file
    __dir__ + '/sounds/' + file
  end

  def play_azan volume, url = nil
    play(url ? url : sound_file_path("2.mp3"), volume)
  end

  def play_reminder volume, url = nil
    play(url ? url : sound_file_path("reminder_1.mp3"), volume)
  end

  def play url, volume
    `amixer sset 'Master' #{volume}`
    `cvlc --play-and-exit #{url}`
  end

  def on_each_minute
    now = Tod::TimeOfDay.parse(Time.now.strftime("%H:%M"))
    times = nil
    # Each day
    if @updated_at.nil? || now == Tod::TimeOfDay.parse('20:00')
      logger.info "Updating the prayer times..."

      @updated_at = now
      begin
        update_prayer_times
        times = prayer_times
        logger.info times.map{|t| {time: t[:time].to_s, before_reminder_at: t[:before_reminder_at].to_s, volume: t[:volume]}}
      rescue
        logger.info "Unable to fetch the prayer times from the remote server!"
        return
      end
    end
    times = prayer_times if times.nil?

    times.each_with_index do |t, i|
      next if i == 1 # Doha

      if t[:time] == now
        play_azan(t[:volume])
      elsif t[:before_reminder_at] == now
        play_reminder(t[:volume])
      end
    end
  end
end
