require 'logger'
require_relative 'azan_coordinator'

class Coordinator
  attr_accessor :l

  def initialize
    log_file_path = __dir__ + '/logs/azan.log'
    @l = Logger.new(log_file_path, 10, 100 * 1024 * 1024) # 100 MB
  end

  def connect_to_speaker
    l.info "-------> Connecting to the speacker"

    l.info `bluetoothctl power on`
    l.info `bluetoothctl agent on`
    2.times do
      l.info `pulseaudio -k`
      l.info `pulseaudio --start`
      l.info `bluetoothctl connect 0E:91:E6:01:1C:22`
    end
  end

  def connect_to_the_internet
    true while !system('ping -c 1 8.8.8.8') # wait till there is a network connection
  end

  def get_azan_coordinator
    azan_configs = {
      default_volume: "80%",
      salawat: [
        {before_reminder: 5, volume: "15%"},
        {},
        {before_reminder: 5},
        {before_reminder: 5},
        {before_reminder: 5},
        {before_reminder: 5},
      ]
    }

    azan_coordinator = AzanCoordinator.new(azan_configs, l)
  end

  def execution_loop
    azan_coordinator = get_azan_coordinator

    sleep 10
    azan_coordinator.play_reminder '90%'

    while true
      azan_coordinator.on_each_minute

      sleep(1) while Time.now.strftime('%S') != '00'
      sleep(1)
      l.info '.'
    end
  end

  def coordinate
    connect_to_the_internet
    connect_to_speaker
    execution_loop
  end
end

c = Coordinator.new
c.coordinate

=begin
- Read the configurations
- Ensure that you're connected to the internet
- Download the latest code if any
- run the ./coordinator.rb
`sh /home/pi/azan/connect_to_speaker.sh'

=end
